#!/usr/bin/env python3
"""
Retrieve attached files from current Spip website
"""

import os
from ruamel.yaml import YAML
import shutil
import subprocess


yaml = YAML(typ='safe')
DOWNLOAD_DIR = os.path.join("attachments", "spip")

if __name__ == '__main__':

    config_filename = "config.yml"
    with open(config_filename, 'r') as ymlfile:
        cfg = yaml.load(ymlfile)

    url = cfg['site_url']

    if os.path.exists(DOWNLOAD_DIR):
        shutil.rmtree(DOWNLOAD_DIR)

    os.makedirs(DOWNLOAD_DIR)

    for directory in "IMG", "Documents":
        dest_dir = os.path.join(DOWNLOAD_DIR, directory)
        cmd = f"""wget --quiet \
                       --show-progress \
                       --no-parent \
                       --recursive \
                       --reject "index.html*" \
                       --exclude-directories="IMG/distant" \
                       --directory-prefix={DOWNLOAD_DIR} \
                       --no-host-directories \
                       --execute robots=off \
                       --continue \
                       {url}/{directory}/"""
        cmd = ' '.join(cmd.split())  # Remove duplicated spaces
        print(f"Downloading {directory} to {dest_dir} using command line:\n{cmd}")
        subprocess.run(cmd, shell=True)  # shell=True is required to avoid " expansion
