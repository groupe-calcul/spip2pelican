#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Split YAML file into separate files and clean them
"""

import argparse
import codecs
from colorlog import ColoredFormatter
import itertools as it
import logging
import os
import re
from ruamel.yaml import YAML
from ruamel.yaml.reader import Reader
import io
import shutil
import sys
import ftfy # Fixing broken encoding
import chardet

SPIPFILES = [
    "spip_auteurs.yml",
    "spip_auteurs_liens.yml",
    "spip_articles.yml",
    "spip_breves.yml",
    "spip_documents.yml",
    "spip_rubriques.yml"
]

OUTPUTDIR = "spip_yml"

TITLETOREMOVE = frozenset([
    'surtitre',
    'soustitre',
    'chapo',
    'maj',
    'export',
    'visites',
    'referers',
    'popularite',
    'accepter_forum',
    'date_modif',
    'langue_choisie',
    'id_trad',
    'id_version',
    'nom_site',
    'url_site',
    'virtuel',
    'date_redac'
])

logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)

# create console handler with higher log level and colored output
ch = logging.StreamHandler(sys.stdout)
ch.setLevel(logging.DEBUG)
LOGFORMAT = "%(log_color)s%(message)s%(reset)s"
color_formatter = ColoredFormatter(LOGFORMAT)
ch.setFormatter(color_formatter)

# add the handlers to logger
logger.addHandler(ch)


def strip_invalid(s):
    """
    Filter characters not allowed by YAML specifications
    """
    res = ''
    for x in s:
        if Reader.NON_PRINTABLE.match(x):
            continue
        res += x

    return res


def escape_title(s):
    """
    Add simple quotes to text titles to avoid interpretation of ":" as yaml syntax
    """

    match = escape_title.pattern.match(s)
    if match and match.group(1) in ["titre", "nom_site", "texte"] and not match.group(2).startswith("|-"):
        return "  {}: '{}'\n".format(match.group(1), match.group(2).replace("'", "''"))
    else:
        return s

escape_title.pattern = re.compile(r'^  ([^ :]+):\s+(.*?)$')


def fix_encoding(s):
    """
    Fix wrong encoded utf-8 lines.
    It is sometimes not consistent: it can fix encoding in a subpart of a
        sentence but not of the whole sentence.
    """
    return ftfy.fix_text(
        s,
        remove_terminal_escapes=True,
        fix_encoding=True,
        fix_entities=True,
        uncurl_quotes=False,
        fix_latin_ligatures=True,
        fix_character_width=True,
        fix_line_breaks=False,
        fix_surrogates=True,
        remove_control_chars=True,
        remove_bom=True,
        normalization='NFC'
    )


def is_invalid_date(s):
    """
    True if title line contains a null date.
    """
    return is_invalid_date.pattern.match(s)

is_invalid_date.pattern = re.compile(r"^  (date|en_ligne)[^:]*: 0000-00-00 00:00:00$")


def remove_null_date(s):
    """
    Remove "date:",  "date_tmp:", etc. if equals to 0000-00-00 00:00:00 (otherwise yaml.load() would fail)
    """
    s = re.sub(r'(^  date|^  en_ligne).*: 0000-00-00 00:00:00$', r'', s, flags=re.MULTILINE)
    return s

def is_removable_title(s):
    """
    True if the title can be remove from the YAML content.
    """
    match = is_removable_title.pattern.match(s)

    return match and match.group(1) in TITLETOREMOVE

is_removable_title.pattern = re.compile(r'^  ([^ :]+):')


def clean_generator(iterable):
    """
    Generator of cleaned and fixed YAML lines.
    """
    #for line in deblock_generator(iterable):
    for line in iterable:
        if is_invalid_date(line) or is_removable_title(line):
            continue

        # Unicode "LINE SEPARATOR" (U+2028) generates error while parsing YAML
        yield escape_title(fix_encoding(line).replace("\u2028", ''))


def clean_yaml(yml_filename):
    """
    Read a given file and write a cleaned version with _clean suffix.
    """

    base_filename = os.path.splitext(yml_filename)[0]
    output_filename = base_filename + "_clean.yml"
    logger.info(f">>> Cleaning {yml_filename} -> {output_filename}")

    with open(yml_filename, mode='r') as yml_in:
        with open(output_filename, mode='w') as yml_out:
            for line in clean_generator(yml_in):
                yml_out.write(line)


def reset_output_directory():
    """Erase existing output files and create empty output directories"""
    if os.path.exists(OUTPUTDIR):
        logger.info(f">>> Cleaning output directory: ./{OUTPUTDIR}/")
        shutil.rmtree(OUTPUTDIR)

    os.makedirs(OUTPUTDIR)


def split_yaml(yml_filename):
    """Split Spip YAML file into SPIPFILES"""

    logger.info(f">>> Splitting {yml_filename} into:")

    with codecs.open(yml_filename, "r", encoding='utf-8', errors='ignore') as yml_file:
        filename = ''
        filepath = ''
        for key, group in it.groupby(yml_file, lambda line: line.startswith('# ')):
            if key:
                filename = f"{list(group)[0].split('.')[1].strip()}.yml"
                filepath = os.path.join(OUTPUTDIR, filename)
            if not key and filename in SPIPFILES:
                logger.info(f"  {filename}")
                content = "".join(list(group))
                with open(filepath, 'w') as yml_subfile:
                    yml_subfile.write(content)


def clean_yaml_files(yml_files):
    """Clean SPIPFILES"""
    for yml_filename in yml_files:
        yml_filepath = os.path.join(OUTPUTDIR, yml_filename)
        clean_yaml(yml_filepath)


def parse_cl_args():
    """Parse command line arguments"""
    parser = argparse.ArgumentParser(description="Split and clean Spip YAML file")
    parser.add_argument('spipfile', metavar='spipfile', type=str, help="Spip YAML filename")
    return parser.parse_args()


if __name__ == '__main__':
    args = parse_cl_args()
    reset_output_directory()
    split_yaml(args.spipfile)
    clean_yaml_files(SPIPFILES)

