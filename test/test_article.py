from spip2pelican import Website


def test_website():

    Website(config_file="test/config.yml")


def test_read_spip():

    website = Website(ml_type='md', include_breves=True,
                      config_file="test/config.yml")
    website.read_spip()


def test_export():
    website = Website(ml_type='md', config_file="test/config.yml")
    website.read_spip()
    website.export_to_pelican()
