#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Convert spip YAML content to markdown following
https://github.com/nhoizey/spip2markdown/blob/master/spip2markdown_options.php
"""

import anytree
import argparse
from ruamel.yaml import YAML
import bs4
from bs4 import BeautifulSoup
from colorlog import ColoredFormatter
import ftfy
import logging
import os
import pypandoc
import re
import shutil
import sys
from process_yaml import strip_invalid, remove_null_date


yaml = YAML(typ='safe')

SKIP_REASON = {"skip_rub": "belonging to a skipped rubrique",
               "empty": "empty content",
               "unpub": "not published"}
SHORTEN = {'article': 'art',
           'rubrique': 'rub',
           'breve': 'brev',
           'message': 'mess'}

CONTENTDIR = 'content'
LOGFILE = 'spip2pelican.log'
logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)

# create file handler which logs up to info messages
fh = logging.FileHandler(LOGFILE, mode='w')
fh.setLevel(logging.INFO)

# create console handler with higher log level and colored output
ch = logging.StreamHandler(sys.stdout)
ch.setLevel(logging.DEBUG)
LOGFORMAT = "%(log_color)s%(message)s%(reset)s"
color_formatter = ColoredFormatter(LOGFORMAT)
ch.setFormatter(color_formatter)

# add the handlers to logger
logger.addHandler(fh)
logger.addHandler(ch)


class Article:
    """
    A generic class for a single Spip article or rubrique to be converted
    into a Pelican article file
    """

    def __init__(self, spip_article, spip_type, website):

        self.type = spip_type
        self.website = website
        id_tag = 'id_' + self.type
        self.short_id = spip_article[id_tag]
        self.id = f"{SHORTEN[self.type]}{self.short_id}"
        self.title = spip_article['titre']
        self.popularity = float(spip_article.get('popularite', 0.))

        self.rubrique = spip_article['id_rubrique']

        rubrique_category = self.website.rubrique_to_category[self.rubrique]
        if self.type == 'rubrique':
            self.category = rubrique_category
        else:
            # type is article or breve
            types = self.type + 's'
            article_category = self.website.categories[types].get(
                               self.short_id, 'spip_divers')
            if article_category != 'spip_divers':
                self.category = article_category
            else:
                self.category = self.website.categories[types].get(
                                self.short_id, rubrique_category)

        if self.category == 'spip_divers':
            logger.warning(f"Article belongs to spip_divers {self.id}: "
                           f"{self.title}")

        if self.category == 'skip':
            self.skip_reason = SKIP_REASON["skip_rub"]
        elif not spip_article['texte'] and not self.type == 'rubrique':
            self.skip_reason = SKIP_REASON["empty"]
        elif spip_article['statut'] != 'publie':
            self.skip_reason = SKIP_REASON["unpub"]
        else:
            self.skip_reason = ''

        if not self.skip_reason:

            self.prefix = f"spip_{self.type}-{self.short_id}"
            self.path = os.path.join(self.category,
                                     f"{self.prefix}.{self.website.ml_type}")
            try:
                self.date = spip_article['date']
            except KeyError:
                self.date = spip_article['date_heure']

            self.modified = spip_article.get('date_modif', self.date)
            self.summary = spip_article.get('descriptif', None)
            self.text = spip_article['texte']

            try:
                self.authors = self.website.author_index[self.id]
            except KeyError:
                self.authors = self.website.default_author

            self.tags = [self.type]

    def get_header(self):
        pass

    def convert(self, s):
        pass

    def convert_title(self, title):
        """strip to remove any line break at end of string"""
        return self.convert(title).strip()

    def export_to_pelican(self):
        """
        Content of a markdown article should look like:
        title: My super title
        date: 2010-12-03 10:20
        modified: 2010-12-05 19:30
        category: Python
        tags: pelican, publishing
        slug: my-super-post
        authors: Alexis Metaireau, Conan Doyle
        summary: Short version for index and feeds

        This is the content of my super blog post.
        """

        logger.info(f"{self.type}_{self.short_id}: {self.title}")

        if self.skip_reason:
            logger.warning(f"  WARNING: skipping because {self.skip_reason}")
        else:
            self.title = self.convert_title(self.title)
            content = ftfy.fix_encoding(self.convert(self.text))
            markdown = self.get_header() + content

            export_path = os.path.join(CONTENTDIR, self.path)
            with open(export_path, 'w') as f:
                f.write(markdown)
                logger.debug(f"  --> {export_path}")

        return self.skip_reason


class ArticleMd(Article):
    """A single Spip article or rubrique to be converted into Pelican"""

    def get_header(self):
        """Return header in md format"""
        header = f"""\
title: {self.title}
date: {self.date}
modified: {self.modified}
category: {self.category}
tags: {self.tags}
slug: {self.prefix}
authors: {self.authors}
summary: {self.summary}

"""
        return header

    def convert(self, s):
        """Convert string from Spip format to Pelican markdown format"""
        s = self.document(s)
        s = self.html_link(s)
        s = self.html_img(s)
        s = self.bold_spaces(s)
        s = self.italic(s)
        s = self.bold(s)
        s = self.italic_spaces(s)
        s = self.italic_firstspace(s)
        s = self.bold(s)
        s = self.ordered_list(s)
        s = self.unordered_list(s)
        s = self.header(s)
        s = self.header_extended(s)
        s = self.horizontal_rule(s)
        s = self.link(s)
        return s

    def document(self, s):
        """
        SPIP: <doc|path> or <img|path>
        md: [text](url) or ![](img)
        """
        def doc_replace(matchobj):
            """A call back function to replace a Spip doc by a Pelican link"""
            doc_type = matchobj.group(1)
            doc_id = int(matchobj.group(2))
            url = os.path.join(self.website.attachments_prefix, "IMG",
                               self.website.doc_index[doc_id])
            docname = os.path.basename(url)
            if doc_type == 'doc':
                return f"[{docname}]({url})"
            else:
                return f"![{docname}]({url})"

        return re.sub(r'<(doc|img)([0-9]+)\|.*>', doc_replace, s)

    def html_link(self, s):
        """Replace html href by the right Pelican (relative) URL"""

        def link_replace(matchobj):
            """
            A call back function to replace a Spip absolute link
            by a relative link to Pelican file
            """

            spip_type = matchobj.group(1)
            id_art = int(matchobj.group(2))
            anchorobj = re.match(r"#(.*)", matchobj.group(3))
            if anchorobj:
                new_url = anchorobj.group(0)
            else:
                new_url = f"spip_{spip_type}-{id_art}.html"
            return new_url

        def link_replace_doc(matchobj):
            """Prepend attachment document path with attachment prefix"""
            return self.website.attachments_prefix + matchobj.group(2)

        soup = bs4.BeautifulSoup(s, "html.parser")
        for link in soup.find_all('a'):
            link_url = link.get('href')
            if link_url:
                new_url = re.sub(r"\A{}/spip.php\?(article|rubrique)([0-9]+)(.*)".format(self.website.site_url),
                                 link_replace,
                                 link_url)
                new_url = re.sub(r"\A({}/|)(Documents/.*)".format(
                          self.website.site_url), link_replace_doc, new_url)
                link['href'] = new_url

        # formatter=None to avoid ">" -> "&gt;" conversion
        return soup.prettify(formatter=None)

    def html_img(self, s):
        """Replace html img src by the right Pelican (relative) URL"""

        def src_replace(matchobj):
            """Prepend attachment image path with attachment prefix"""
            return self.website.attachments_prefix + matchobj.group(0)

        soup = bs4.BeautifulSoup(s, "html.parser")
        for img in soup.find_all('img'):
            img_src = img.get('src')
            if img_src:
                new_src = re.sub(r"\ADocuments/.*", src_replace, img_src)
                img['src'] = new_src

        # formatter=None to avoid ">" -> "&gt;" conversion
        return soup.prettify(formatter=None)

    @staticmethod
    def bold_spaces(s):
        """
        SPIP: {{ ... }}
        md: **...**
        """
        return re.sub(r"(^|[^{]){{ ([^}]+) }}([^}]|$)", r"\1**\2**\3", s)

    @staticmethod
    def italic(s):
        """
        SPIP: {...}
        md: *...*
        """
        return re.sub(r"(^|[^{]){([^}]+)}([^}]|$)", r"\1*\2*\3", s)

    @staticmethod
    def bold(s):
        """
        SPIP: {{...}}
        md: **...**
        """
        return re.sub(r"(^|[^{]){{([^}]+)}}([^}]|$)", r"\1**\2**\3", s)

    @staticmethod
    def italic_spaces(s):
        """
        SPIP: { ... }
        md: *...*
        """
        return re.sub(r"(^|[^{]){ ([^}]+) }([^}]|$)", r"\1*\2*\3", s)

    @staticmethod
    def italic_firstspace(s):
        """
        SPIP: { ... }
        md: *...*
        """
        return re.sub(r"(^|[^{]){ ([^}]+)}([^}]|$)", r"\1*\2*\3", s)

    @staticmethod
    def ordered_list(s):
        """
        SPIP: - or -# in 1rst level, -## for second level, etc.
        md: 1. with 4-space indents
        """
        # Add a carriage return to separate from a possible non listed element
        s = re.sub(r"(^|\n)-# ", r"\n\g<1>1. ", s)
        # --- Remove excessive carriage return
        s = re.sub(r"\n1. ([^\n]*)\n\n1. ", r"\n1. \1\n1. ", s)
        s = re.sub(r"\n1. ([^\n]*)\n\n1. ", r"\n1. \1\n1. ", s)
        # ---
        s = re.sub(r"(^|\n)-## ", r"\1    1. ", s)
        s = re.sub(r"(^|\n)-### ", r"\1        1. ", s)
        s = re.sub(r"(^|\n)-#### ", r"\1            1. ", s)
        return s

    @staticmethod
    def unordered_list(s):
        """
        SPIP: - or -* in 1rst level, -** for second level, etc.
        md: - with 4-space indents
        """
        # Add a carriage return to separate from a possible non listed element
        s = re.sub(r"(^|\n)-\*? ", r"\n\1- ", s)
        # --- Remove excessive carriage return
        s = re.sub(r"\n- ([^\n]*)\n\n- ", r"\n- \1\n- ", s)
        s = re.sub(r"\n- ([^\n]*)\n\n- ", r"\n- \1\n- ", s)
        # ---
        s = re.sub(r"(^|\n)-\*\* ", r"\1    - ", s)
        s = re.sub(r"(^|\n)-\*\*\* ", r"\1        - ", s)
        s = re.sub(r"(^|\n)-\*\*\*\* ", r"\1            - ", s)
        return s

    @staticmethod
    def header(s):
        """
        SPIP: {{{...}}}
        md: ## ...
        """
        return re.sub(r"(^|[^{]){{{([^}]+)}}}([^}]|$)", r"\1\n## \2\n\3", s)

    @staticmethod
    def header_extended(s):
        """
        SPIP: {{{{{...}}}}}
        md: ### ...
        """
        return re.sub(r"(^|[^{]){{{{{([^}]+)}}}}}([^}]|$)", r"\1\n### \2\n\3", s)

    @staticmethod
    def horizontal_rule(s):
        """
        SPIP: ---- with no carriage return before and after
        md: \n---\n
        """
        s = re.sub(r"<hr>", r"\n---\n", s)
        s = re.sub(r"----", r"\n---\n", s)
        return s

    def link(self, s):
        """
        SPIP: [text->url] or [text -> url]
        md: [text](url) or <url> if text is empty
        """
        def link_replace(matchobj):
            """A call back function to replace a Spip link by a Pelican link"""

            text = matchobj.group(1).strip()
            url = matchobj.group(2).strip()

            def nullify_url(id_art, text, url):
                """Throw WARNING message and return empty URL"""
                msg = f"  WARNING: nullify link to non existing article {id_art}\n"
                msg += f"    text: {text}\n"
                msg += f"    url:  {url}"
                logger.warning(msg)
                self.website.nullified_urls += 1
                return ""

            # Remove "mailto:" prefix from URL
            url = re.sub(r"\Amailto:", "", url)

            if text == '' or text == url:
                # <url>
                new_link = f"<{url}>"
            else:
                # [text->url]
                if re.match(r"\Ahttp", url) or re.match(r"(^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$)", url):
                    # [text->http://] or [text->https://] or
                    # [text->email@subdomain.domain]
                    new_url = url
                else:
                    doc_url = re.match(r"\Adoc([0-9]+)", url)
                    art_url = re.match(r"\A(art|rub|brev)([0-9]+)", url)
                    if doc_url:
                        # [text->doc#]
                        new_url = os.path.join(self.website.attachments_prefix,
                                               "IMG",
                                               self.website.doc_index[int(doc_url.group(1))])
                    elif art_url:
                        # [text->art#,rub#,brev#]
                        art_id = art_url.group(0)
                        try:
                            new_url = "{filename}/" + \
                                      self.website.article_index[art_id]
                        except KeyError:
                            new_url = nullify_url(art_id, text, url)
                    else:
                        # [text->path_to_file]
                        new_url = os.path.join(self.website.attachments_prefix,
                                               url)

                if new_url:
                    new_link = f"[{text}]({new_url})"
                else:
                    new_link = f"{text}"  # URL is nullified
            return new_link

        return re.sub(r"\[([^\]]*)->([^\]]+)\]", link_replace, s)


class ArticleRst(Article):
    """A single Spip article or rubrique to be converted into Pelican"""

    def get_header(self):
        """Return header in rst format"""
        header = f"""\
:title: {self.title}
:date: {self.date}
:modified: {self.modified}
:category: {self.category}
:tags: {self.tags}
:slug: {self.prefix}
:authors: {self.authors}
:summary: {self.summary}

"""
        return header

    def convert(self, s):
        """Convert string from Spip format to Pelican markdown format"""
        # # s = self.html_link(s)
        # # s = self.html_img(s)
        s = self.ordered_list(s)
        s = self.unordered_list(s)
        # s = self.horizontal_rule(s)
        s = self.fix_li(s)
        s = self.convert_html(s)
        s = self.remove_font(s)
        s = self.bold(s)
        s = self.italic(s)
        s = self.link(s)
        s = self.remove_space(s)
        s = self.remove_empty_link(s)
        s = self.document(s)
        s = self.fix_table(s)
        s = self.remove_blank(s)
        s = self.header(s)
        s = self.header_extended(s)
        return s

    def document(self, s):
        """
        SPIP: <doc|path> or <img|path>
        md: [text](url) or ![](img)
        """

        def doc_rst(match):
            doc_type = match[1]
            doc_id = int(match[2])
            url = os.path.join(self.website.attachments_prefix,
                               "IMG",
                               self.website.doc_index[doc_id])
            docname = os.path.basename(url)
            if doc_type == 'doc':
                return f'`{docname} <{url}>`__'
            else:
                return f'\n\n..image:: {url}\n\n'

        regex = re.compile(r'<(doc|img)([0-9]+)\|.*>')
        return regex.sub(doc_rst, s)

    def html_link(self, s):
        """Replace html href by the right Pelican (relative) URL"""

        def link_replace(matchobj):
            """
            A call back function to replace a Spip absolute link
            by a relative link to Pelican file
            """

            spip_type = matchobj.group(1)
            id_art = int(matchobj.group(2))
            anchorobj = re.match(r"#(.*)", matchobj.group(3))
            if anchorobj:
                new_url = anchorobj.group(0)
            else:
                new_url = f"spip_{spip_type}-{id_art}.html"
            return new_url

        def link_replace_doc(matchobj):
            """Prepend attachment document path with attachment prefix"""
            return self.website.attachments_prefix + matchobj.group(2)

        soup = bs4.BeautifulSoup(s, "html.parser")
        for link in soup.find_all('a'):
            link_url = link.get('href')
            if link_url:
                new_url = re.sub(r"\A{}/spip.php\?(article|rubrique)([0-9]+)(.*)".format(self.website.site_url),
                                 link_replace,
                                 link_url)
                new_url = re.sub(r"\A({}/|)(Documents/.*)".format(self.website.site_url), link_replace_doc, new_url)
                link['href'] = new_url

        # formatter=None to avoid ">" -> "&gt;" conversion
        return soup.prettify(formatter=None)

    def html_img(self, s):
        """Replace html img src by the right Pelican (relative) URL"""

        def src_replace(matchobj):
            """Prepend attachment image path with attachment prefix"""
            return self.website.attachments_prefix + matchobj.group(0)

        soup = bs4.BeautifulSoup(s, "html.parser")
        for img in soup.find_all('img'):
            img_src = img.get('src')
            if img_src:
                new_src = re.sub(r"\ADocuments/.*", src_replace, img_src)
                img['src'] = new_src

        # formatter=None to avoid ">" -> "&gt;" conversion
        return soup.prettify(formatter=None)

    def fix_table(self, s):
        def remove_bad_char(match):
            return '| |'

        regex = re.compile(r'\|(\^|<)\|')
        s = regex.sub(remove_bad_char, s)
        return re.sub(r'\|', '', s)

    def fix_li(self, s):
        soup = BeautifulSoup(s, 'html.parser')

        for li in soup.find_all('li'):
            if isinstance(li.contents[0], str):
                text = li.contents[0].replace('\n', '')
                li.replace_with(text)

        return soup.prettify(formatter=None)

    def remove_space(self, s):
        new = []
        for l in s.split("\n"):
            new.append(l.strip())
        return '\n'.join(new)

    def remove_blank(self, s):
        new = []
        for l in s.split("\n"):
            if l.lstrip().startswith('-'):
                new.append(l + '\n')
            else:
                new.append(l.lstrip())
        return '\n'.join(new)

    def convert_html(self, lines):
        soup = BeautifulSoup(lines, 'html.parser')

        for html in soup.find_all('ul'):
            s = pypandoc.convert_text(html, 'rst', format='html',
                                      extra_args=['--wrap=preserve'])
            html.replace_with(s)

        for html in soup.find_all('a'):
            s = pypandoc.convert_text(html, 'rst', format='html',
                                      extra_args=['--wrap=preserve'])
            html.replace_with(s)

        return soup.prettify(formatter=None)

    @staticmethod
    def remove_font(s):
        def font_rst(match):
            return ' '

        regex = re.compile(r'(<font .*>)')
        s = regex.sub(font_rst, s)

        regex = re.compile(r'(</font>)')
        s = regex.sub(font_rst, s)

        regex = re.compile(r'(<html>)')
        s = regex.sub(font_rst, s)

        regex = re.compile(r'(</html>)')
        s = regex.sub(font_rst, s)

        regex = re.compile(r'(<hr/>)')
        return regex.sub(font_rst, s)

    @staticmethod
    def bold(s):
        """
        SPIP: {{ ... }}
        md: **...**
        """

        def bold_rst(match):
            text = match[2].strip()
            return f'**{text}** '

        regex = re.compile(r'({{2})([^}]+)(}{2})')

        new = []
        for l in s.split("\n"):
            new.append(regex.sub(bold_rst, l))
        return '\n'.join(new)

    @staticmethod
    def italic(s):
        """
        SPIP: {...}
        md: *...*
        """

        def italic_rst(match):
            text = match[2].strip()
            return f'*{text}* '

        regex = re.compile(r'({)([^}]*)(})')

        new = []
        for l in s.split("\n"):
            new.append(regex.sub(italic_rst, l))
        return '\n'.join(new)

    @staticmethod
    def ordered_list(s):
        """
        SPIP: - or -# in 1rst level, -## for second level, etc.
        md: 1. with 4-space indents
        """

        def ordered_rst(match):
            indent = ' ' * 4 * (match[1].count('*') - 1)
            return f'\n{indent}- {match[2]}\n'

        regex = re.compile(r'^\s*-\s*(\#*)(.*)')

        new = []
        for l in s.split("\n"):
            new.append(regex.sub(ordered_rst, l))
        return '\n'.join(new)

    @staticmethod
    def remove_empty_link(s):
        def replace(match):
            return f'{match[1]}'

        regex = re.compile(r'`(.*)<>`__')
        return regex.sub(replace, s)

    @staticmethod
    def unordered_list(s):
        """
        SPIP: - or -* in 1rst level, -** for second level, etc.
        md: - with 4-space indents
        """

        def unordered_rst(match):
            indent = ' ' * 4 * (match[1].count('*') - 1)
            text = match[2].strip()
            return f'\n{indent}- {text}\n'

        regex = re.compile(r'^\s*-\s*(\**)(.*)')

        new = []
        for l in s.split("\n"):
            new.append(regex.sub(unordered_rst, l))
        return '\n'.join(new)

    @staticmethod
    def header(s):
        """
        SPIP: {{{...}}}
        md: ## ...
        """

        def header_rst(match):
            text = match[2].strip()
            return text + '\n' + '=' * len(text) + '\n'

        regex = re.compile(r'({{3})([^}]*)(}{3})')

        return regex.sub(header_rst, s)

    @staticmethod
    def header_extended(s):
        """
        SPIP: {{{{{...}}}}}
        md: ### ...
        """

        def header_rst(match):
            text = match[2].strip()
            return text + '\n' + '-' * len(text) + '\n'

        regex = re.compile(r'({{5})([^}]*)(}{5})')

        return regex.sub(header_rst, s)

    @staticmethod
    def horizontal_rule(s):
        """
        SPIP: ---- with no carriage return before and after
        md: \n---\n
        """
        s = re.sub(r"<hr>", r"\n---\n", s)
        s = re.sub(r"----", r"\n---\n", s)
        return s

    def link(self, s):
        """
        SPIP: [text->url] or [text -> url]
        md: [text](url) or <url> if text is empty
        """

        def nullify_url(id_art, text, url):
            """Throw WARNING message and return empty URL"""
            msg = f"  WARNING: nullify link to non existing article {id_art}\n"
            msg += f"    text: {text}\n"
            msg += f"    url:  {url}"
            logger.warning(msg)
            self.website.nullified_urls += 1
            return ""

        def link_rst(match):
            text = match[1]
            link = match[2].strip()

            if text == '' or text == link:
                return f'{link}'

            email = re.match('mailto:(.*)', link)
            if email:
                return f'{email.group(1).strip()}'

            http_url = re.match(r'http', link)
            if http_url:
                return f'`{text} <{link}>`__'

            doc_url = re.match(r'doc([0-9]+)', link)
            if doc_url:
                link = os.path.join(self.website.attachments_prefix, "IMG",
                                    self.website.doc_index[int(doc_url.group(1))])
                return f'`{text} <{link}>`__'

            art_url = re.match(r"(art|rub|brev)([0-9]+)", link)
            if art_url:
                art_id = art_url.group(0)
                try:
                    link = self.website.article_index[art_id]
                except KeyError:
                    link = nullify_url(art_id, text, link)
                return f'`{text} <{link}>`__'

            link = os.path.join(self.website.attachments_prefix, link)
            return f'`{text} <{link}>`__'

        regex = re.compile(r'\[([^]]*)\s*-\s*>\s*([^]]*)\]')
        return regex.sub(link_rst, s)


class Website:
    """Define a website content from Spip data"""

    DEFAULT_AUTHOR = "Webmaster"
    ATTACHMENTS_PREFIX = "attachments/spip/"
    WORK_DIR = "."
    SPIP_DIR = "spip_yml"

    @staticmethod
    def _load_and_clean_yaml(filename):
        """Load yaml file filename, clean it and return a dictionary"""
        with open(filename, mode='r') as yml_file:
            return yaml.load(remove_null_date(strip_invalid(yml_file)))

    def __init__(self, reset_output_dir=True, include_breves=False,
                 ml_type='md', config_file='config.yml'):

        self.reset_output_dir = reset_output_dir
        self.include_breves = include_breves
        self.ml_type = ml_type

        self.rubrique_to_category = {}
        self.article_index = {}
        self.doc_index = {}

        self.author_index = {}
        self.nullified_urls = 0
        self.articles = []

        self.rubrique_nodes = {}
        self.rubrique_tree = None

        with open(config_file, 'r') as ymlfile:
            cfg = yaml.load(ymlfile)

        # Set attributes from config.yml file or use default values
        self.site_url = cfg['site_url']
        self.default_author = cfg.get('default_author', self.DEFAULT_AUTHOR)
        self.work_dir = cfg.get('work_dir', self.WORK_DIR)
        self.attachments_prefix = cfg.get('attachments_prefix',
                                          self.ATTACHMENTS_PREFIX)
        self.spip_dir = os.path.join(self.work_dir,
                                     cfg.get('spip_dir', self.SPIP_DIR))

        self.rubriques_filename = os.path.join(self.spip_dir,
                                               "spip_rubriques_clean.yml")
        self.articles_filename = os.path.join(self.spip_dir,
                                              "spip_articles_clean.yml")
        self.breves_filename = os.path.join(self.spip_dir,
                                            "spip_breves_clean.yml")
        self.documents_filename = os.path.join(self.spip_dir,
                                               "spip_documents_clean.yml")
        self.authors_filename = os.path.join(self.spip_dir,
                                             "spip_auteurs_clean.yml")
        self.authors_links_filename = os.path.join(self.spip_dir,
                                                   "spip_auteurs_liens_clean.yml")

        self.categories = {}

        def get_categories(spip_type):
            """Return {spip_rubrique: pelican_category} for given spip_type"""
            categories = {}
            try:
                cfg_categories = cfg['categories'][spip_type]
                for pelican_category, spip_id in cfg_categories.items():
                    if type(spip_id) == int:
                        # this pelican category corresponds
                        # to a single rubrique
                        categories[spip_id] = pelican_category
                    elif type(spip_id) == list:
                        # this pelican category corresponds
                        # to a list of rubriques
                        for rubrique in spip_id:
                            categories[rubrique] = pelican_category
                    else:
                        logger.critical(f"Error in {config_file}: "
                                        f"{pelican_category}: {spip_id}")
            except KeyError:
                logger.warning(f"No category description for {spip_type} "
                               f"in {config_file}")
            finally:
                categories[-1] = "spip_divers"
            return categories

        self.categories['rubriques'] = get_categories('rubriques')
        self.categories['articles'] = get_categories('articles')
        if self.include_breves:
            self.categories['breves'] = get_categories('breves')

        if self.ml_type == 'md':
            self.Article = ArticleMd
        elif self.ml_type == 'rst':
            self.Article = ArticleRst
        else:
            exit(f'Unknown markup language type: {self.ml_type}.')

    def _reset_output_directories(self):
        """Erase existing output files and create empty output directories"""
        if os.path.exists(CONTENTDIR):
            shutil.rmtree(CONTENTDIR)
        for category in set(self.categories['rubriques'].values()):
            if category != 'skip':
                os.makedirs(os.path.join(CONTENTDIR, category))

    def _build_rubrique_to_category(self):
        """Build the index dictionary: {id_rubrique: category_name}"""

        def get_category(id_rubrique):
            """Return category from id_rubrique"""
            while id_rubrique not in self.categories['rubriques']:
                try:
                    id_rubrique = self.parents[id_rubrique]
                except KeyError:
                    id_rubrique = -1

            return self.categories['rubriques'][id_rubrique]

        # Load original rubriques file as a list
        with open(self.rubriques_filename, mode='r') as yml_file:
            rubriques = yaml.load(yml_file.read())

        self.parents = {rubrique['id_rubrique']: rubrique['id_parent']
                        for rubrique in rubriques}
        self.labels = {rubrique['id_rubrique']: rubrique['titre'].strip()
                       for rubrique in rubriques}
        self.rubrique_to_category = {rubrique['id_rubrique']:
                                     get_category(rubrique['id_rubrique'])
                                     for rubrique in rubriques}

    def print_rubrique_tree(self):
        """Print the rubrique structure as a tree using anytree"""

        def get_label(node_id):
            """Render node label from node id"""
            return f"{node_id}: {self.labels[node_id]}"

        def insert_node(node_id):
            """
            A recursive function to insert a node in the anytree.AnyNode tree
            """
            node_name = get_label(node_id)
            if node_name not in self.rubrique_nodes:
                # Create node only if it does not exist
                if node_id == 0:
                    # This the root node
                    self.rubrique_nodes[node_name] = anytree.AnyNode(name=node_name,
                                                                     id=node_id,
                                                                     count=0)
                else:
                    parent_id = self.parents[node_id]
                    parent_name = get_label(parent_id)
                    parent = self.rubrique_nodes.get(parent_name)
                    if not parent:
                        # insert parent if it does not exist
                        insert_node(parent_id)
                        parent = self.rubrique_nodes[parent_name]
                    self.rubrique_nodes[node_name] = anytree.AnyNode(name=node_name,
                                                                     id=node_id,
                                                                     count=0,
                                                                     parent=parent)

        self.labels[0] = "root"  # Add root node label

        for node_id in self.parents:
            insert_node(node_id)

        # rubrique_tree is the root node
        self.rubrique_tree = self.rubrique_nodes[get_label(0)]
        # Count the number of pages for each rubrique
        for article in self.articles:
            rubrique_node = anytree.search.find_by_attr(self.rubrique_tree,
                                                        article.rubrique,
                                                        name="id")
            rubrique_node.count += 1

        logger.debug("-------")
        logger.debug("Spip rubriques scheme:")
        logger.debug("id: title [number of pages]")
        # Render the AnyNode object like the bash "tree" command would do
        for pre, fill, rubrique_node in anytree.RenderTree(self.rubrique_nodes['0: root']):
            logger.info(f"{pre}{rubrique_node.name} [{rubrique_node.count}]")

    def print_articles(self, id_rubrique, spip_type):
        """Print a list of given spip_type articles"""
        logger.info(f"Spip {spip_type}s that belong to rubrique {id_rubrique}: "
                    f"{self.labels[id_rubrique]}")
        for article in self.articles:
            if article.rubrique == id_rubrique and article.type == spip_type:
                logger.info(f"            - {article.short_id}  # {article.title}")

    def sort_articles(self):
        """Print a list of given spip_type articles"""
        logger.info(f"Spip articles sorted by popularity:")
        selected_articles = [article for article in self.articles
                             if not article.skip_reason]
        popularities = [article.popularity for article in selected_articles]
        sorted_articles = [article for _, article
                           in reversed(sorted(zip(popularities,
                                                  selected_articles),
                                              key=lambda x: x[0]))]
        for article in sorted_articles:
            logger.info(f"{article.path} {article.popularity:.5f}")

    def _build_doc_index(self):
        """Build the index dictionary: {id_doc: file_path}"""

        # Load original document file as a list
        docs = self._load_and_clean_yaml(self.documents_filename)
        self.doc_index = {doc['id_document']: doc['fichier'] for doc in docs}

    def _build_author_index(self):
        """Build the index dictionary: {spip_type: art_id: author_name}"""

        # Load author file as a list
        authors = self._load_and_clean_yaml(self.authors_filename)
        author_name_index = {author['id_auteur']: author['nom']
                             for author in authors}

        # Load article/author file as a list
        authors_links = self._load_and_clean_yaml(self.authors_links_filename)

        for authors_link in authors_links:
            spip_type = authors_link['objet']
            # art_id = art23, rub45, brev98, etc.
            art_id = f"{SHORTEN[spip_type]}{(authors_link['id_objet'])}"
            author_id = authors_link['id_auteur']
            self.author_index[art_id] = author_name_index[author_id]

    def _build_articles(self):
        """Instanciate a list of Article objects"""

        def add_articles(filename, spip_type):
            """
            Add article or rubrique to website.articles and
            website.article_index
            """
            # Load original article file as a list
            with open(filename, mode='r') as yml_file:
                spip_articles = yaml.load(yml_file)
                for spip_article in spip_articles:
                    article = self.Article(spip_article, spip_type, self)
                    self.articles.append(article)
                    if not article.skip_reason:
                        self.article_index[article.id] = article.path

        add_articles(self.articles_filename, 'article')
        add_articles(self.rubriques_filename, 'rubrique')
        if self.include_breves:
            add_articles(self.breves_filename, 'breve')

    def read_spip(self):
        """Read spip yaml files to build useful indices and article list"""
        logger.debug("-------")
        logger.debug("Loading Spip data")
        self._build_rubrique_to_category()
        self._build_doc_index()
        self._build_author_index()
        self._build_articles()

    def export_to_pelican(self):
        """Loop on Spip articles to convert them into Pelican format"""

        logger.debug("-------")
        logger.debug("Exporting to Pelican")

        if self.reset_output_dir:
            self._reset_output_directories()

        processed = []
        for article in self.articles:
            skip_reason = article.export_to_pelican()
            processed.append(skip_reason)

        logger.info("-------")
        logger.info("Summary")
        logger.info(f"  {len(processed)} processed articles:")
        logger.info(f"    {processed.count('')} converted articles")
        for k in SKIP_REASON:
            logger.warning(f"    {processed.count(SKIP_REASON[k])} skipped "
                           f"articles because {SKIP_REASON[k]}")
        logger.warning(f"    {self.nullified_urls} nullified URLs")
        logger.info("-------")
        logger.debug(f"See {LOGFILE}")


def parse_cl_args():
    """Parse command line arguments"""
    description = "Convert Spip website as YAML data to Pelican format"
    parser = argparse.ArgumentParser(description=description)
    parser.add_argument('-r', '--rubriques', action='store_true',
                        help="Show Spip rubriques structure as a tree")
    parser.add_argument('-a', '--articles', metavar="id_rubrique", nargs=1,
                        help="List Spip articles corresponding to given rubrique id")
    parser.add_argument('-b', '--breves', metavar="id_rubrique", nargs=1,
                        help="List Spip breves corresponding to given rubrique id")
    parser.add_argument('-c', '--convert', action='store_true', default=True,
                        help="Convert to Pelican")
    parser.add_argument('-cf', '--config', metavar="config",
                        default='config.yml', type=str,
                        help="Configuration file")
    parser.add_argument('-ib', '--include_breves', action='store_true',
                        help="Include Spip breves in conversion")
    parser.add_argument('-ml', '--markup', metavar="language", default='md',
                        type=str, help="Set markup language (md or rst)")
    parser.add_argument('-p', '--popularite', action='store_true',
                        default=False,
                        help="Sort articles and rubriques according to popularity")
    return parser.parse_args()


if __name__ == '__main__':
    args = parse_cl_args()
    website = Website(include_breves=args.include_breves, ml_type=args.markup,
                      config_file=args.config)
    website.read_spip()
    if args.rubriques:
        # Show only Spip rubrique structure
        website.print_rubrique_tree()
    elif args.articles:
        # Show a list of articles that belongs to id_rubrique
        id_rubrique = int(args.articles[0])
        website.print_articles(id_rubrique, 'article')
    elif args.breves:
        # Show a list of breves that belongs to id_rubrique
        id_rubrique = int(args.breves[0])
        website.print_articles(id_rubrique, 'breve')
    elif args.popularite:
        # Show a list of articles sorted according to popularity
        website.sort_articles()
    else:
        website.export_to_pelican()
