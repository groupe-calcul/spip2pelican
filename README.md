# Conversion d'un site Spip en site Pelican

## Dépendances

- `wget`
- `python 3.6` (pour le support de fstring)
- les paquets python listés dans `requirements.txt` à installer avec :

```bash
pip install -r requirements.txt
```

## Utilisation

### 1. Récupérer les fichiers joints

Le script suivant aspire les répertoires `IMG/` et `Documents/` du site Spip pour récupérer les fichiers qui ne sont pas des pages web :

```
python3 get_attached_files.py
```

Les fichiers récupérés sont dans `attachments/spip/`

### 2. Exporter la base de données Spip

1. Se connecter à la [base mysql](http://calcul.math.cnrs.fr/phpmyadmin/index.php?db=spip_calcul) du site spip avec phpmyadmin
2. Exporter la table globale du site en format `yaml`, par exemple dans le fichier `spip_site.yml`

### 3. Nettoyer l'export `yaml`

Depuis le répertoire de ce projet, lancer le script `process_yaml.py` pour découper en sous fichiers yaml et nettoyer l'encodage des caractères :

```
python3 process_yaml.py spip_site.yml
```

Ce script produit les fichiers `./spip_yml/spip_*_clean.yml`.

### 4. Définir la stratégie de conversion des rubriques et articles Spip en catégories Pélican

- Explorer la structure des rubriques du site Spip

```
python3 spip2pelican.py --rubriques
```

- Lister le contenu en articles des rubriques afin de déterminer la correspondance `spip_article -> pelican_category` ou `spip_rubrique -> pelican_category`

```
python3 spip2pelican.py --articles id_rubrique
```


### 5. Convertir les articles Spip en articles Pelican

- Editer le fichier `config.yml`, en particulier le dictionnaire `categories` qui opère la conversion des rubriques Spip en *category* Pelican en fonction du schéma déterminé dans l'étape précédente.

- Lancer la conversion:

```
python3 spip2pelican.py
```

### 6. Transférer les fichiers markdown exportés vers le nouveau site

```
rsync -av content/ ../website/content/
```

### 7. Transférer les fichiers joints vers le nouveau site

```
rsync -av --delete attachments/spip/ ../website/content/attachments/spip/
```

## Notes

- les balises html ne sont pas converties car elles sont supportées par la syntaxe markdown, seuls les champs `href` et `img` sont traités

